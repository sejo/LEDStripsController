// LEDStripsController
//
// A simple Arduino sketch to control several LED strips with the same power source
// José Vega-Cebrián (Sejo) - jmvc[at]escenaconsejo.org - www.escenaconsejo.org
// April 2016
//
// This code is in the public domain
//
// LED strips should be connected through an appropriate set of MOSFETs (N-Channel in this case)
//
// Only N_STRIPS_ON strip(s) is/are on at a given time.
// The ON state is alternated at the given TOTAL_FREQUENCY


// ***************************************************
// CONFIGURABLE DEFINITIONS 
// ***************************************************
// Total number of strips
#define N_STRIPS 	8

// Pin number of first strip
#define FIRST_STRIP 	37

// Pin number separation between strips
#define STRIPS_SEP 	2

// Number of simultaneously turned on strips
#define N_STRIPS_ON	1

// Duty cycle (% of period the strip will be ON)
#define DUTY_CYCLE	100	

// Frequency of the whole cycle (Hz)
#define TOTAL_FREQUENCY	100	

// Definition of the voltages to turn on/off a strip
#define STRIP_OFF	LOW
#define STRIP_ON	HIGH

// ***************************************************
// CONSTANT DEFINITIONS 
// ***************************************************
// Period of the whole cycle (ms)
#define TOTAL_PERIOD 1000/TOTAL_FREQUENCY

// Period corresponding to one strip
#define STRIP_PERIOD TOTAL_PERIOD/N_STRIPS

// Period corresponding to one strip turned ON
#define STRIP_PERIOD_ON STRIP_PERIOD*DUTY_CYCLE/100


// ***************************************************
// GLOBAL VARIABLES
// ***************************************************
// Array of pin numbers where strips are connected
int strips[N_STRIPS]; 

// Pointer to the active strip
int active_strip;

// The millis value at the start of the strip period
unsigned long prev_millis;

// Millis difference between millis and prev_millis
unsigned long dif_millis;


// ***************************************************
// FUNCTION DEFINITIONS
// ***************************************************
//
// Turn off all the strips
void turnAllOff();

// Turn on the given strip
void turnStripOn(int si);

// Turn off the given strip
void turnStripOff(int si);



// ***************************************************
// PROGRAM
// ***************************************************
void setup(){

	// Array and pins initialization
	for(int i=0;i<N_STRIPS;i++){
		// Start from first strip, add the separation
		strips[i] = FIRST_STRIP + STRIPS_SEP*i;

		// Set pin as output
		pinMode(strips[i], OUTPUT);

		// Turn it off
		digitalWrite(strips[i],STRIP_OFF);
	}

	// Initialize the active strip pointer
	active_strip = 0;

	// Set the start of the new period
	prev_millis = millis();

}

void loop(){
	// Calculate difference
	dif_millis = millis()-prev_millis;

	// If the strip period has finished...
	if(dif_millis > STRIP_PERIOD){
		// Set the start of the new period
		prev_millis = millis();

		// Turn off the active strip
		turnStripOff(active_strip);

		// Increment the active strip and wrap the pointer around
		active_strip = (active_strip+1) % N_STRIPS;

		// Turn on the new active strip and the simultaneous ones 
		for(int i=0;i<N_STRIPS_ON;i++){
			turnStripOn(active_strip+i);
		}


	}

	// If the strip ON period has finished...
	else if(dif_millis > STRIP_PERIOD_ON){
		// Turn off the given strip
		turnStripOff(active_strip);
	}


}

// ***************************************************
// FUNCTION IMPLEMENTATION
// ***************************************************
void turnAllOff(){
	for(int i=0;i<N_STRIPS;i++){
		digitalWrite(strips[i], STRIP_OFF);	
	}
}

void turnStripOn(int si){
	digitalWrite(strips[si], STRIP_ON);	
}

void turnStripOff(int si){
	digitalWrite(strips[si], STRIP_OFF);	
}
