# LEDStripsController
A simple Arduino sketch to control several LED strips with the same power source.

## Description
LED strips require great currents. A strategy to turn many of them on without requiring big power sources is to divide them into sections and assign time slots to each one. In each time slot only one of the sections is turned on. If this process is carried on at a "high" frequency, the human eye is tricked and we see all the sections on. 


## Notes
* LED strips should be connected through an appropriate set of MOSFETs (N-Channel in this case)
* Only `N_STRIPS_ON` strip(s) is/are on at a given time.
* The ON state is alternated at the given `TOTAL_FREQUENCY`

## License
This code in the public domain
